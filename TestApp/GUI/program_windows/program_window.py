from abc import ABC, abstractmethod

class ProgramWindow(ABC):
	###Experimental
	__master_window = None
	###
	
	def __init__(self, parent = None, controller = None):
		self.parent = parent
		self.controller = controller
		
		self.parent_x_button_deactivate()
		self.window = self._create_window()
		self.program_master_window = self.window
		self.x_button_activate()
	
	@property
	def program_master_window(self):
		return ProgramWindow.__master_window
	@program_master_window.setter
	def program_master_window(self, window):
		if ProgramWindow.__master_window == None:
			ProgramWindow.__master_window = window
			return
	
	@property
	def window(self):
		return self._window
	@window.setter
	def window(self, window_instance):
		print(f'ProgramWindow: new {type(window_instance).__name__} window created')
		allowed = ('Tk', 'Toplevel')
		if not type(window_instance).__name__ in allowed:
			raise ValueError(f'ProgramWindow.window should be a {" or ".join(allowed)} instance!')
		
		self._window = window_instance
		
	def parent_x_button_deactivate(self):
		if not self.parent == None:
			self.parent.x_button_deactivate()
	
	def parent_x_button_activate(self):
		if not self.parent == None:
			self.parent.x_button_activate()
	
	###Experimental
	def master_show(self):
		if not self.program_master_window == None:
			self.program_master_window.deiconify()
	def master_withdraw(self):
		if not self.program_master_window == None:
			self.program_master_window.withdraw()
	####
	
	def close_window(self):
		self.parent_x_button_activate()
		self.window.update_idletasks()
		self._destroy_window_sequence()
	
	@abstractmethod
	def x_button_deactivate(self): pass
	
	@abstractmethod
	def x_button_activate(self): pass
		
	@abstractmethod
	def _create_window(self):
		'''Creates the program window.
		
		Must create one and only one window.
		Must return the tkinter.Tk() instance of that window
		'''
		pass
		
	@abstractmethod
	def _destroy_window_sequence(self): pass
	


if __name__ == '__main__':
	import tkinter
	class TestWindow(ProgramWindow):
		def x_button_activate(self):
			print(self, 'x_button_activate')
			self.root.protocol('WM_DELETE_WINDOW', self.close_window)
		
		def x_button_deactivate(self):
			print(self, 'x_button_deactivate')
			self.root.protocol('WM_DELETE_WINDOW', lambda: None)
			
		def _create_window(self):
			self.root = tkinter.Tk()
		
		def _destroy_window(self):
			self.root.destroy()
			
	root = TestWindow()
	child = TestWindow(root)
	tkinter.mainloop()
	print('done')
