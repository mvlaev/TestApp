from Logic.tests.test import Test

###TODO: ADD RAW TEST: Test how long is the raw data
###

#class ValidDataRaw(Test):
#	test_name = 'Data RAW Test'
#	test_type = 'ZeroTestCommon'
#	
#	def calculate(self):
#		if data.raw == None:
#			return 'FAIL'
#		return 'OK'


class ValidDataId(Test):
	test_name = 'Data ID Test'
	test_type = 'ZeroTestCritical'
	
	def calculate(self):
		if self.data.data_id == None:
			return 'ERROR'
		return 'OK'
	
	def create_custom_message(self):
		if self.result.test_status == 'ERROR':
			return 'CRITICAL! The main controller is giving a bad line!'
		return self._DEFAULT_OK_MESSAGE

class ValidDataMess(Test):
	test_name = 'Data Mess Test'
	test_type = 'ZeroTestCritical'
	
	def calculate(self):
		db_empty = self.data.base.count(None) == len(self.data.base)
		readings_not_empty = not self.data.months.count(None) == len(self.data.months)
		comments_not_empty = not self.data.comments.count(None) == len(self.data.comments)
		
		if db_empty and (readings_not_empty or comments_not_empty):
			return 'ERROR'
		return 'OK'
	
	def create_custom_message(self):
		if self.result.test_status == 'ERROR':
			return 'CRITICAL! Empty database with non empty readings or comments!'
		return self._DEFAULT_OK_MESSAGE
