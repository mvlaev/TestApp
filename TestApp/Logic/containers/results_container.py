from Logic.CONSTANTS import TEST_TYPES_HIERARCHY, TEST_STATUSES_HIERARCHY

class ResultsContainer:
	def __init__(self, data_type):
		self.data_type = data_type
		self.completed = []
		
#		self.ok_count = 0
#		self.warning_count = 0
#		self.error_count = 0
#		self.fail_count = 0
	@property
	def all_results(self):
		return len(self.completed)
	@property
	def okeys(self):
		return len(self.list_by_test_status('OK'))
	@property
	def warnings(self):
		return len(self.list_by_test_status('WARNING'))
	@property
	def errors(self):
		return len(self.list_by_test_status('ERROR'))
	@property
	def fails(self):
		return len(self.list_by_test_status('FAIL'))
	@property
	def skips(self):
		return len(self.list_by_test_status('SKIP'))
	
	def list_by_test_status(self, *test_statuses):
		if not test_statuses:
			test_statuses = ('FAIL', 'ERROR', 'WARNING')
		return self.__sort([r for r in self.completed if r.test_status in test_statuses])
	
	def list_by_test_type(self, *test_types):
		return self.__sort([r for r in self.completed if r.test_type in test_types])
	
	def list_all(self):
		return self.__sort(self.completed)
	
	def str_by_test_status(self, *test_statuses):
		return [str(result) for result in self.list_by_test_status(*test_statuses)]
	
	def str_by_test_type(self, *test_types):
		return [str(result) for result in self.list_by_test_type(*test_types)]
		
	def str_all(self):
		return [str(result) for result in self.list_all()]
	
	def summary(self):
		return f'''\

SUMMARY:
Total number of results for "{self.data_type}": {self.all_results}

"FAIL"    : {self.fails}
"ERROR"   : {self.errors}
"WARNING" : {self.warnings}
"OK"      : {self.okeys}
"SKIP"    : {self.skips}

'''
	
	def __sort(self, what):
	###Sorts by data_id, test_types_hierarchy, status_hierarchy: FAIL, ERROR, WARNING, OK
		by_status  = sorted(what,      key = lambda x: TEST_STATUSES_HIERARCHY[x.test_status])
		by_type    = sorted(by_status, key = lambda x: TEST_TYPES_HIERARCHY[x.test_type])
		by_data_id = sorted(by_type,   key = lambda x: x.data_id)
		
		return by_data_id
		
	def __str__(self):
		return '\n'.join([self.summary(), *self.str_all()])
	
	def add_result(self, result): #result should be of type TestResult
		self.completed.append(result)
		
		

