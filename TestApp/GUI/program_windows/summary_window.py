from tkinter import *
from tkinter import ttk

from GUI.program_windows.program_window import ProgramWindow
from GUI.callbacks.summary_callbacks import SummaryCallbacks

class SummaryWindow(ProgramWindow):
	
	def x_button_deactivate(self):
		self.summary_window.protocol('WM_DELETE_WINDOW', lambda: None)
	
	def x_button_activate(self):
		self.summary_window.protocol('WM_DELETE_WINDOW', self.close_window)
	
	def _create_window(self):
		'''Creates the program window.
		
		Must create one and only one window.
		Must return the tkinter.Tk() instance of that window
		'''
		self.summary_window = Toplevel(self.parent.window)
		self.summary_window.transient(self.parent.window)
		self.summary_window.wait_visibility()
		
		self.summary_window.grab_set()
		self.summary_window.resizable(height=False, width=False)
		self.summary_window.title('Finished')
		
		
		
		summary_frame = ttk.Frame(self.summary_window, width=250, height=100, padding='5 5')
		summary_frame.grid()
		
		message = 'Process completed with no runtime errors.'
		self.summary_label = ttk.Label(summary_frame, text=message)
		self.summary_label.grid(pady=5, columnspan=3, sticky='swen')
		
		summary_info_label_frame = ttk.LabelFrame(summary_frame, text='Short Summary', padding='5 5 5 5')
		summary_info_label_frame.grid(columnspan=3, sticky='swen', pady=5, padx=5)
		
		short_info = 'Tests: \nErrors: \nWarnings: '
#		self.summary_info_label_frame_label = ttk.Label(summary_info_label_frame, text=short_info)
#		self.summary_info_label_frame_label.grid()
		self.summary_info_label_frame_text = Text(summary_info_label_frame, width=40, height=12, wrap=WORD)
		self.summary_info_label_frame_text.grid(padx=5, pady=5, sticky='swen')
		
		self.summary_results_button = ttk.Button(summary_frame, text='Show Results...', command=lambda: SummaryCallbacks.show_results_log(self))
		self.summary_results_button.grid(column=0, row=5, pady=5, padx=5)
		
		self.summary_control_button = ttk.Button(summary_frame, text='Show System Log...', command=lambda: SummaryCallbacks.show_control_log(self))
		self.summary_control_button.grid(column=1, row=5, pady=5, padx=5)
		
		self.summary_close_button = ttk.Button(summary_frame, text='Done', command=self.close_window)
		self.summary_close_button.grid(column=2, row=5, pady=5, padx=5)
		
		SummaryCallbacks.set_values(self)
		self.summary_window.update_idletasks()
		return self.summary_window
	
	def _destroy_window_sequence(self):
		self.summary_window.destroy()
