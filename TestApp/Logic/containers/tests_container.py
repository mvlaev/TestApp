###Import all zero tests:
import Logic.tests.zero_tests.zero_tests_critical
###

###Import all database tests:
import Logic.tests.database_tests.skip_line_database
import Logic.tests.database_tests.common_database_tests
#TODO: IMPORT THE REST....
#.........
###

###Import all data tests:
import Logic.tests.data_tests.data_tests
import Logic.tests.data_tests.skip_line_electro
import Logic.tests.data_tests.skip_line_water
#..........
###

###Import the abstract test class:
from Logic.tests.test import Test
###

from Logic.CONSTANTS import TESTS_BY_DATA_TYPE

class TestsContainer:
	def __init__(self, data_type):
		#Test type names for given data type:
		critical_tests 	   = TESTS_BY_DATA_TYPE[data_type]['CriticalTests']
		skip_line_database = TESTS_BY_DATA_TYPE[data_type]['SkipLineDatabase']
		skip_line_data	   = TESTS_BY_DATA_TYPE[data_type]['SkipLineReadings']
		data_tests 		   = TESTS_BY_DATA_TYPE[data_type]['DataTests']
		database_tests 	   = TESTS_BY_DATA_TYPE[data_type]['DatabaseTests']
		
		self.__critical_tests     = [test for test in Test.__subclasses__() if test.test_type in critical_tests]
		self.__skip_line_database = [test for test in Test.__subclasses__() if test.test_type in skip_line_database]
		self.__skip_line_data     = [test for test in Test.__subclasses__() if test.test_type in skip_line_data]
		self.__data_tests         = [test for test in Test.__subclasses__() if test.test_type in data_tests]
		self.__database_tests     = [test for test in Test.__subclasses__() if test.test_type in database_tests]
		
	@property
	def critical_test_classes(self):
		return self.__critical_tests
	@property
	def skip_line_database_classes(self):
		return self.__skip_line_database
	@property
	def skip_line_data_classes(self):
		return self.__skip_line_data
	@property
	def data_test_classes(self):
		return self.__data_tests
	@property
	def database_test_classes(self):
		return self.__database_tests
