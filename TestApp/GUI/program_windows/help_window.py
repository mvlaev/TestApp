from tkinter import *
from tkinter import ttk

from GUI.program_windows.program_window import ProgramWindow

class HelpWindow(ProgramWindow):
	
	def x_button_deactivate(self): pass
	
	
	def x_button_activate(self):
		self.help_window.protocol('WM_DELETE_WINDOW', self.close_window)
	
	def _create_window(self):
		'''Creates the program window.
		
		Must create one and only one window.
		Must return the tkinter.Tk() instance of that window
		'''
		self.parent.x_button_activate()
		
		self.help_window = Toplevel(self.parent.window)
		self.help_window.title('Help')
		self.help_window.columnconfigure(0, weight=1)
		self.help_window.rowconfigure(0, weight=1)
		#self.help_window.geometry('300x300')
		
		help_frame = ttk.Frame(self.help_window, width=300, height=300)
		help_frame.columnconfigure(0, weight=1)
		help_frame.rowconfigure(0, weight=1)
		help_frame.grid(column=0, row=0, sticky='swen')
		
		###TODO : GET THE HELP FILE VIA THE CONTROLLER
		info = 'Here must be displayed the contents of the Help File!'
		
		#Setting a text widget
		help_text = Text(help_frame, wrap='word')
		help_text.grid(sticky='swen')
		help_text.insert('end', info)
		help_text['state'] = 'disabled'
		
		###Add scrollbars
		y_scroll = ttk.Scrollbar(help_frame, orient='vertical', command=help_text.yview)
		y_scroll.grid(column=1, row=0, sticky='ns')
		
		help_text['yscrollcommand'] = y_scroll.set
		
		
		
#		self.help_label = ttk.Label(help_frame, text=info, padding='5 5')
#		self.help_label.grid(column=0, row=0, columnspan=2)
		
#		close_button = ttk.Button(help_frame, text='Close', command=self.close_window)
#		close_button.grid(column=0, row=1)
		
#		#To DO : credits button command
#		credits_button = ttk.Button(help_frame, text='Credits', command=self.close_window)
#		credits_button.grid(column=1, row=1)
		
		
		return self.help_window
	
	def _destroy_window_sequence(self):
#		self.parent.window.deiconify()
		self.help_window.destroy()
