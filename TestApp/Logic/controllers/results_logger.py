from logging import getLogger
from pathlib import Path
from time    import asctime, strftime

from Logic.CONSTANTS import LOG_TYPES, LOG_PATH, DEFAULT_FILENAMES_BY_LOG_TYPE


class LogCreate:
	def __init__(self, log_type = None, filename = None):
		self.log_type = log_type
		self.__logger = getLogger('LogCreate')
		
		if not filename:
			if not log_type in LOG_TYPES:
				self.__logger.error(f'Cannot create file. Log type \'{log_type}\' must be one of the following: {", ".join(LOG_TYPES)}.')
				raise ValueError(f'Log type \'{log_type}\' must be one of the following: {", ".join(LOG_TYPES)}.')
			
			filename = Path(LOG_PATH, DEFAULT_FILENAMES_BY_LOG_TYPE[log_type])
			
		else:
			filename = Path(LOG_PATH, filename).resolve()
		
		self.file = open(filename, 'w')
		self.file.write(f'<< {log_type} >> {strftime("%d.%m.%Y / %X")}\n')
		
		self.__logger.info(f'Created {filename}.')
		
	def write_data(self, data): self.file.write(data)
	def close(self):
		self.file.close()
		self.__logger.info(f'{self.log_type} closed.')

#TODO: LOG THE READER, PLEASE?
class LogRead:
	def __init__(self, log_type = None, filename = None):
		self.__logger = getLogger('LogRead')
		
		if not filename:
			if not log_type in LOG_TYPES:
				self.__logger.error(f'Cannot read file. Log type \'{log_type}\' must be one of the following: {", ".join(LOG_TYPES)}.')
				raise ValueError(f'Log type \'{log_type}\' must be one of the following: {", ".join(LOG_TYPES)}.')
			
			self.filename = Path(LOG_PATH, DEFAULT_FILENAMES_BY_LOG_TYPE[log_type])
			
		else:
			self.filename = Path(LOG_PATH, filename).resolve()
	
	def read_log(self):
		self.__logger.info(f'Reading {self.filename}...')
		###Read the log
		with open(self.filename, 'r') as f:
			content = f.readlines()
#		content.pop(0) #pop the file header
		content = ''.join(content)
		
		self.__logger.info(f'Done.')
		return content
	
	def save_as(self, output_file_path : str):
		####Debug
		self.__logger.info(f'Renaming file "{self.filename}"...')
		####
		
		self.filename = self.filename.rename(Path(output_file_path).resolve())
		
		####Debug
		self.__logger.info(f'Done. New filename: {self.filename}')
		####
		return self.filename
	
#	def discard(self):
#		######
#		self.__logger.warning(f'Truncating: {self.filename}')
#		######
#		
#		self.filename.write_text(f'Data truncated on: {asctime()}')
#		
#		self.__logger.warning('Truncating done.')

