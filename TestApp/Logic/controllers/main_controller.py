from logging import getLogger
from multiprocessing import Process, SimpleQueue
#from pathlib import Path
from time import asctime, sleep, monotonic

from Logic.CONSTANTS import LOG_PATH, DEFAULT_FILENAMES_BY_LOG_TYPE

from Logic.controllers.control_logger import client_configurer
from Logic.controllers.tests_controller import TestsController
from Logic.controllers.results_logger import LogRead, LogCreate
from Logic.controllers.xlsx_reader import XLSXReader


def _procedure(stop_signal, log_queue, **kwargs):
	'''Procedure for working on the tests.
	
	stop_signal:multiprocessing.Queue
	
	**kwargs:
	do_data_tests:bool
	do_database_tests:bool
	do_water_tests:bool
	do_electro_tests:bool
	input_filename:str
	prev_month:int
	'''
		
	#############################################################
	def write_results(tests_controller, line_count):
		results_container = tests_controller.results
		
		if not line_count > 0:
			error_msg = f'''
{"Data type":10s}: {results_container.data_type}
An error occurred! No data present!
Please check the System Log file for more information.
'''
			logger.error('An error occurred! No data present!')
			results_log.write_data(error_msg)
			summary_log.write_data(error_msg)
			
			logger.warning('Continue...')
			return
			
			
		
		to_write = '\n' + results_container.summary() + '\n'.join(results_container.str_by_test_status())
#		to_write = str(results_container)
		
		logger.info(f'Writing {results_container.all_results} results of {results_container.data_type}.')
		
		results_log.write_data(to_write)
		
		logger.info('Done.')
		
		to_write = f'''
{"Data type":10s}: {results_container.data_type}
{"Lines":10s}: {line_count}
{"Results":10s}: {results_container.all_results}
{"Very Bad":10s}: {results_container.fails + results_container.errors}
'''

		logger.info('Writing SUMMARY.')
		summary_log.write_data(to_write)
		
		logger.info('Done.')
		#######################################
	
	#############################################################
	def orchestrator(data_type):
		enter_time = monotonic()
		
		logger.info(f'Starting xlsx_reader for {data_type}')
		data_container = xlsx_reader.get_data(data_type)
		
		if stop_now(): return
		
		logger.info(f'Starting tests_controller for {data_type}')
		tests_controller = TestsController(do_data_tests     = kwargs['do_data_tests'],
				                           do_database_tests = kwargs['do_database_tests'],
				                           data_type         = data_type)
		
		logger.info(f'Running tests for {data_type}')
		for line in data_container:
			#TODO: CHECK FOR STOP?
			#TODO: LOG STOP?
			tests_controller.step(line)
		
		if stop_now(): return
		
		logger.info('Writing results')
		write_results(tests_controller, len(data_container))
		
		logger.info(f'Done with {len(data_container)} lines of {data_type} in {monotonic() - enter_time:.4f} sec.')
	
	#############################################################
	def stop_now():
		if stop_signal.empty():
			return False
		###TODO: Log a stop_signal event
		logger.warning('Got stop signal! Stopping procedure...')
		return True
	#######################################################
	def clean_and_close():
		if not stop_signal.empty():
			logger.warning('Procedure stopped by the user.')
		
		while not stop_signal.empty():
			stop_signal.get()
		stop_signal.close()
		
		logger.info('Closing xlsx_reader...')
		xlsx_reader.close()
		
		logger.info('Closing results log writer...')
		results_log.close()
		
		logger.info('Closing summary log writer...')
		summary_log.close()
	#######################################################
	
	#############################################################
	###Marking the start time:
	time_mark = monotonic()
	
	###CONFIGURE LOGGING:
	client_configurer(log_queue)
	logger = getLogger('main_controller._procedure')
	#####################################
	
	####LOGGING:
	logger.info(f'Procedure started successfully at {asctime()}.')
	logger.info('Creating XLSXReader instance...')
	###########################
	xlsx_reader = XLSXReader(input_filename   = kwargs['input_filename'],
	                         prev_month       = kwargs['prev_month'])
	
	logger.info('Creating logs...')
	##################################
	results_log = LogCreate('ResultsLog')
	summary_log = LogCreate('SummaryLog')
	#####DEBUG##################
	
	if stop_now():
		clean_and_close()
		return
	
	if kwargs['do_electro_tests']: orchestrator('ElectroData')
	
	if stop_now():
		clean_and_close()
		return
	
	if kwargs['do_water_tests']  : orchestrator('WaterData')
	
	clean_and_close()
	#####LOGGING:
	logger.info(f'All done in {monotonic() - time_mark:.4f} sec. at {asctime()}')
	###############################


class MainController:
	def __init__(self, log_queue):
		
		self.__logger 		             = getLogger('main_controller')
		self.running_processes           = []
		self.stop_signal 	             = None
		self.custom_control_log_filename = None
		self.custom_results_log_filename = None
		
		self.__log_queue 	             = log_queue
		self.__success                   = True
		
		self.__logger.info('MainController initiated!')
	
	@property
	def done(self):
		if any([p.is_alive() for p in self.running_processes]):
			return False
		return True
	
	@property
	def success(self):
		if self.done:
			if any([not p.exitcode == 0 for p in self.running_processes]):
				return False
			
			[p.close() for p in self.running_processes]
			self.running_processes = []
			return True
		raise RuntimeError('MainController not DONE!')
	
	
#######Procedure control:
#TODO: SOME LOGGING HERE?
	def start_procedure(self, **kwargs):
		'''Recieves all the parameters.
		
		**kwargs:
		do_data_tests:bool
		do_database_tests:bool
		do_water_tests:bool
		do_electro_tests:bool
		input_filename:str
		prev_month:int
		'''
		
		self.__logger.info('Preparing to start procedure...')
		
		if not self.done:
			self.__logger.error(f'''Can not start tests until the running ones are done!
Still running:
{" ".join([str(p) for p in self.running_processes])}''')
			return
		
		#self.custom_control_log_filename = None
		self.custom_results_log_filename = None
		
		self.stop_signal = SimpleQueue()
		p = Process(name='Procedure', target=_procedure, args=(self.stop_signal, self.__log_queue), kwargs=kwargs)
		self.running_processes = [p]
		
		self.__logger.info('Starting procedure...')
		for p in self.running_processes: p.start()
		
		
	def stop_procedure(self):
		if self.done: return
		
		self.stop_signal.put('STOP')
		
		while not self.done:
			
			sleep(0.01)
		
		###TODO: LOG THE SUCCESSFUL STOP
		print('all stopped')
###############################################################################

#######Results control:
	def get_results_log(self):
		if self.custom_results_log_filename:
			return self.__get_log(filename = self.custom_results_log_filename)
		return self.__get_log(log_type = 'ResultsLog')
	
	def get_control_log(self):
		if self.custom_control_log_filename:
			return self.__get_log(filename = self.custom_control_log_filename)
		return self.__get_log(log_type = 'ControlLog')
	
	def get_summary(self):
		return self.__get_log(log_type = 'SummaryLog')
	
	def save_results_log_as(self, filename):
		if self.custom_results_log_filename:
			self.custom_results_log_filename = self.__save_log_as(custom_name     = self.custom_results_log_filename,
			                                                      output_filename = filename)
			return
		self.custom_results_log_filename = self.__save_log_as(log_type        = 'ResultsLog',
		                                                      output_filename = filename)
		return
	
	def save_control_log_as(self, filename):
		if self.custom_control_log_filename:
			self.custom_control_log_filename = self.__save_log_as(custom_name     = self.custom_control_log_filename,
			                                                      output_filename = filename)
			return
		self.custom_control_log_filename = self.__save_log_as(log_type        = 'ControlLog',
		                                                      output_filename = filename)
		return
		
	def __get_log(self, log_type = None, filename = None):
		if not self.done:
			self.__logger.error(f'Can not return log. Procedure still running.')
			return f'Can not return log. Procedure still running.'
		
		if filename:
			log_reader = LogRead(filename = filename)
		elif log_type:
			log_reader = LogRead(log_type = log_type)
		else:
			self.__logger.error(f'Can not return log. No log type nor filename provided!')
			return f'Can not return log. No log type nor filename provided!'
		
		try:
			content = log_reader.read_log()
		except FileNotFoundError:
			return f'File {log_reader.filename} does not exist yet!'
		
		return content
	
	def __save_log_as(self, output_filename, log_type = None, custom_name = None):
		if not self.done:
			self.__logger.error(f'Can not save log. Procedure still running.')
			return 
		
		if custom_name:
			log_reader = LogRead(filename = custom_name)
		elif log_type:
			log_reader = LogRead(log_type = log_type)
		else:
			self.__logger.error(f'Can not save log. No log type nor file to read from provided!')
			return
			
		return log_reader.save_as(output_filename)
		
###############################################################################
###DONE: START
###DONE: STOP
###DONE: SET_PARAMETERS
###TODO: ROUTINE
###DONE: CHECK_STATUS
###DONE: GET_LOG
