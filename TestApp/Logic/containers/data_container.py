#last tested 4 Oct 21
#last mod 7 Oct 21
from Logic.CONSTANTS import DATA_TYPES

class DataContainer:
	def __init__(self, data_type): #data_type should be of the permitted data types
	###TODO: CHECK THE DATA TYPE AGAINST DATA_TYPES
		self.data_type = data_type
		self.__data = []
	
	def __len__(self):
		return len(self.__data)
	
	def __str__(self):
		return f'< {self.data_type} DataContainer > {len(self)} item/s.'
		
	def __iter__(self):
		for d in self.__data:
			yield d
	
	def __delitem__(self, index):
		if not index == 0:
			raise TypeError(f'{type(self).__name__} supports only deletion of the item at position 0.')
		del self.__data[0]
			

			
	def add_data(self, *data_obj):
		self.__data += list(data_obj)
	
if __name__ == '__main__':
	dc = DataContainer('WaterData')
	
	dc.add(str)
	for i in dc: print(type(i).__name__)
	print(dc)
	
	
