import logging
import logging.handlers

from pathlib import Path

from Logic.CONSTANTS import LOG_PATH, DEFAULT_FILENAMES_BY_LOG_TYPE

def master_logger(q, filename = None):
	if filename:
		log_filename = Path(filename).resolve()
	else:
		log_filename = Path(LOG_PATH, DEFAULT_FILENAMES_BY_LOG_TYPE['ControlLog']).resolve()
	
	debug_log_filename = Path(LOG_PATH, DEFAULT_FILENAMES_BY_LOG_TYPE['DebugLog']).resolve()
	
	print(f'logging to {log_filename}\ndebugging in {debug_log_filename}')
	
	root = logging.getLogger()
	root.setLevel(logging.DEBUG)
	
#	stream_handler = logging.StreamHandler()
	file_handler  = logging.FileHandler(log_filename, 'w')
	debug_handler = logging.FileHandler(debug_log_filename, 'w')
	
	formatter 					  = logging.Formatter('{asctime} > {levelname:8s} : {name:35s} > {message}', style='{')
	formatter.default_time_format = '%H:%M:%S'
#	formatter.default_msec_format = '%s,%03d'
	
	root.addHandler(file_handler)
	root.addHandler(debug_handler)
	
	file_handler.setLevel(logging.WARNING)
	file_handler.setFormatter(formatter)
	
	debug_handler.setLevel(logging.DEBUG)
	debug_handler.setFormatter(formatter)
	
	for record in iter(q.get, 'STOP'):
		root.handle(record)
	
def client_configurer(q):
	root 	  = logging.getLogger()
	q_handler = logging.handlers.QueueHandler(q)
	
	root.addHandler(q_handler)
	
	root.setLevel(logging.DEBUG)
	q_handler.setLevel(logging.DEBUG)
	

