from logging import getLogger
from tkinter import filedialog

class ShowLogCallbacks:
	def save_log_as(self, log_type):
		logger = getLogger('ShowSaveLog')
		self.x_button_deactivate()
		
		logger.info('Getting filename...')
		output_filename = filedialog.asksaveasfilename(filetypes={'{Text File} {.txt}'})
		
		self.x_button_activate()
		
		if output_filename:
			if log_type == 'ResultsLog':
				self.controller.save_results_log_as(output_filename)
			elif log_type == 'ControlLog':
				self.controller.save_control_log_as(output_filename)
			else:
				logger.error(f'Error saving to file {output_filename}, log type {log_type} not found!')
				return
			self.save_as_button.state(['disabled'])
			return
		
		logger.warning('Did not get a filename! Returning...')
	
	def get_log(self, log_type):
		if log_type == 'ControlLog':
			return self.controller.get_control_log()
		elif log_type == 'ResultsLog':
			return self.controller.get_results_log()
		
		return f'Error! Log type "{log_type}" not found!'
