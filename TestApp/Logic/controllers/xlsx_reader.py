from logging  import getLogger
from openpyxl import load_workbook
from pathlib  import Path

from Logic.containers.data_container import DataContainer
from Logic.CONSTANTS                 import SHEET_NAMES_BY_DATA_TYPE, SHEET_MAX_COLUMNS_BY_DATA_TYPE, DATA_CLASSES_BY_DATA_TYPE

class XLSXReader:
	def __init__(self, *, input_filename: str, prev_month: int):
		self.filename   = Path(input_filename).resolve()
		self.prev_month = prev_month
		self.__wb       = load_workbook(filename=self.filename, read_only=True)
		self.__logger   = getLogger('XLSXReader')
		
		self.__logger.info('XLSXReader initiated.')
		
	def get_data(self, data_type):
		self.__logger.info('Trying to get data.')
		container = DataContainer(data_type)
		
		try:
			data_class = DATA_CLASSES_BY_DATA_TYPE[data_type]
		except KeyError:
			self.__logger.error(f'ERROR! Could not find Data Class of type {data_type}')
			return container
		
		sheet_name = SHEET_NAMES_BY_DATA_TYPE[data_type]
		
		if not sheet_name in self.__wb.sheetnames:
			self.__logger.error(f'ERROR! Sheet \'{sheet_name}\' not in workbook \'{self.filename}\'!')
			return container
		
		ws = self.__wb[sheet_name]
		###SILENT DIMENSIONS ERROR!!!!!!!!
		dim_0 = ws.calculate_dimension()
		
		ws.reset_dimensions()
		dim = ws.calculate_dimension(force=True)
		
		if not dim_0 == dim:
			self.__logger.warning('Sheet {sheet_name} of file {self.filename} is corrupted - BAD dimensions! Please check your file!')
		
		if not SHEET_MAX_COLUMNS_BY_DATA_TYPE[data_type] == ws.max_column:
			self.__logger.error(f'ERROR! Sheet \'{sheet_name}\' in file \'{self.filename}\' has BAD column count!')
			return container
		
		self.__logger.info('Reading the file and populating the data container.')
		for line in enumerate(ws.values, start=1):
			data = data_class(line, self.prev_month)
			container.add_data(data)
		
		###Spit the first line - LABELS!
		del container[0]
		self.__logger.info('Done.')
		return container
		
	
	def close(self):
		self.__wb.close()
		self.__logger.info('Workbook closed.')
