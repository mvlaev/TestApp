from Logic.tests.test import Test

class ValidPrevMonthValue(Test):
	test_name = 'Data Previous Month Test'
	test_type = 'DataTest'
	
	def calculate(self):
		try:
			float(self.data.previous_month)
		except TypeError:
		###previous_month is None:
			return 'None'
		except ValueError:
		###previous_month is not a valid number:
			return 'NAN'
		else:
			if self.data.previous_month < 0:
				return 'NEGATIVE'
			return 'OK'
	
	def create_custom_message(self):
		if self.result.test_status == 'None':
			self.result.test_status = 'ERROR'
			return 'Previous month reading missing!'
		
		if self.result.test_status == 'NAN':
			self.result.test_status = 'ERROR'
			return 'Previous month reading has bad characters!'
			
		if self.result.test_status == 'NEGATIVE':
			self.result.test_status = 'WARNING'
			return 'Previous month reading is NEGATIVE!'
		
		if self.result.test_status == 'OK':
			return self._DEFAULT_OK_MESSAGE
		
		self.result.test_status = 'FAIL'
		return 'UNKNOWN ERROR HAPPENED!'

class ValidLastMonthValue(Test):
	test_name = 'Data Last Month Test'
	test_type = 'DataTest'
	
	def calculate(self):
		try:
			float(self.data.last_month)
		except TypeError:
		###last_month is None:
			return 'None'
		except ValueError:
		###last_month is not a valid number:
			return 'NAN'
		else:
			if self.data.last_month < 0:
				return 'NEGATIVE'
			return 'OK'
	
	def create_custom_message(self):
		if self.result.test_status == 'None':
			self.result.test_status = 'ERROR'
			return 'Last month reading missing!'
		
		if self.result.test_status == 'NAN':
			self.result.test_status = 'ERROR'
			return 'Last month reading has bad characters!'
		
		if self.result.test_status == 'NEGATIVE':
			self.result.test_status = 'WARNING'
			return 'Last month reading is NEGATIVE!'
		
		if self.result.test_status == 'OK':
			return self._DEFAULT_OK_MESSAGE
		
		self.result.test_status = 'FAIL'
		return 'UNKNOWN ERROR HAPPENED!'

class NegativeConsumption(Test):
	test_name = 'Negative Consumption Test'
	test_type = 'DataTest'
	
	def calculate(self):
		'''Wraps all the calculations.
		
		MUST RETURN A TEST_STATUS_VALUE STRING.
		'''
		try:
			pm = float(self.data.previous_month)
			lm = float(self.data.last_month)
		except (TypeError, ValueError):
			return 'FAIL'
		else:
			if pm <= lm:
				return 'OK'
			return 'NEGATIVE'
	
	def create_custom_message(self):
		'''Creates a custom status message for all possible statuses of the test.
		
		MUST RETURN A STRING.
		'''
		if self.result.test_status == 'FAIL':
			return 'Corrupted values for previous month and/or last month!'
		
		if self.result.test_status == 'NEGATIVE':
			self.result.test_status = 'ERROR'
			return 'Negative consumption!'
		
		if self.result.test_status == 'OK':
			return self._DEFAULT_OK_MESSAGE
		
		return 'UNKNOWN ERROR HAPPENED!'

class AllowedConsumption(Test):
	test_name = 'Allowed Consumption Test'
	test_type = 'DataTest'
	
	def calculate(self):
		'''Wraps all the calculations.
		
		MUST RETURN A TEST_STATUS_VALUE STRING.
		'''
		try:
			pm = float(self.data.previous_month)
			lm = float(self.data.last_month)
			min_consumption = float(self.data.min)
			max_consumption = float(self.data.max)
			
		except (TypeError, ValueError):
			return 'FAIL'
		
		else:
			consumption = (lm - pm) * getattr(self.data, 'transform', 1)
			
			if consumption < min_consumption:
				return 'TOO LOW'
			
			if consumption > max_consumption:
				return 'TOO HIGH'
			
			return 'OK'
			
	def create_custom_message(self):
		'''Creates a custom status message for all possible statuses of the test.
		
		MUST RETURN A STRING.
		'''
		if self.result.test_status == 'FAIL':
			return 'Some of the values are corrupted: previous month / last month / min consumption / max consumption!'
		
		if self.result.test_status == 'TOO LOW':
			self.result.test_status = 'WARNING'
			return 'Consumption is LOWER than expected!'
		
		if self.result.test_status == 'TOO HIGH':
			self.result.test_status = 'WARNING'
			return 'Consumption is HIGHER than expected!'
		
		if self.result.test_status == 'OK':
			return self._DEFAULT_OK_MESSAGE
		
		return 'UNKNOWN ERROR HAPPENED!'
	

