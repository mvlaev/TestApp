from Logic.tests.test import Test

class SkipLineWaterReadings(Test):
	test_name = 'Skip Line Readings Test'
	test_type = 'SkipLineWaterReadings'
	
	def calculate(self):
		if self.data.unit == None:
			return 'SKIP'
##		if 'ost' in self.data.level:
##			return 'SKIP'
#		if self.data.min and type(self.data.min).__name__ == 'str':
#			return 'SKIP'
		
		return 'OK'
	
	def create_custom_message(self):
		if self.result.test_status == 'SKIP':
			return 'Skipping line.'
		
		return self._DEFAULT_OK_MESSAGE

