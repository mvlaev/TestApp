from pathlib import Path
from sys import argv
from Logic.containers.data import ElectroData, WaterData
##############################
DATA_TYPES = 'ElectroData', 'WaterData'

DATA_CLASSES_BY_DATA_TYPE = {
'ElectroData'	:ElectroData,
'WaterData'		:WaterData
}

TESTS_BY_DATA_TYPE = {
'WaterData'		:{	'CriticalTests'		:('ZeroTestCritical',),
					'SkipLineDatabase'	:('SkipLineDatabase',),
					'SkipLineReadings'	:('SkipLineWaterReadings',),
					'DataTests'			:('DataTest',),
					'DatabaseTests'		:('WaterDatabaseTest','CommonDatabaseTest')
				 },
'ElectroData'	:{ 	'CriticalTests'		:('ZeroTestCritical',),
					'SkipLineDatabase'	:('SkipLineDatabase',),
					'SkipLineReadings'	:('SkipLineElectroReadings',),
					'DataTests'			:('DataTest',),
					'DatabaseTests'		:('ElectroDatabaseTest','CommonDatabaseTest')}
}

SHEET_NAMES_BY_DATA_TYPE = {
				'WaterData'		:'Водомери',
				'ElectroData'	:'Електромери'}

SHEET_MAX_COLUMNS_BY_DATA_TYPE = {
				'WaterData'		: 22,
				'ElectroData'	: 25}
##############################

##############################
LOG_TYPES = 'ControlLog', 'ResultsLog', 'SummaryLog'

__main = Path(argv[0]).resolve()
__path_to_main = __main.parent

LOG_PATH = Path(__path_to_main, 'Logs')
if not LOG_PATH.exists(): LOG_PATH.mkdir()

DEFAULT_FILENAMES_BY_LOG_TYPE = {
'ControlLog'	:	'control_log.txt',
'DebugLog'      :   'debug_log.txt',
'ResultsLog'	:	'results_log.txt',
'SummaryLog'    :   'summary_log.txt'}
##############################

##############################
TEST_TYPES = 'CommonDatabaseTest', 'DataTest', 'ElectroDatabaseTest', 'SkipLineDatabase', 'SkipLineElectroReadings', 'SkipLineWaterReadings', 'WaterDatabaseTest', 'ZeroTestCritical'

TEST_TYPES_HIERARCHY = {'CommonDatabaseTest':1, 'DataTest':2, 'ElectroDatabaseTest':1, 'SkipLineDatabase':3, 'SkipLineElectroReadings':3, 'SkipLineWaterReadings':3, 'WaterDatabaseTest':1, 'ZeroTestCritical':0}

TEST_STATUSES = 'FAIL', 'ERROR', 'WARNING', 'OK', 'SKIP'

TEST_STATUSES_HIERARCHY = {'FAIL': 0, 'ERROR': 1, 'WARNING': 2, 'OK':3, 'SKIP':4}
##############################
