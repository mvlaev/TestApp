from tkinter import *
from tkinter import ttk

from GUI.program_windows.program_window import ProgramWindow
from GUI.callbacks.work_progress_callbacks import WorkProgressCallbacks

class WorkProgressWindow(ProgramWindow):
	
	def x_button_deactivate(self):
		self.work_progress_window.protocol("WM_DELETE_WINDOW", lambda : None)
	
	def x_button_activate(self):
		self.x_button_deactivate()
	
	def _create_window(self):
		self.work_progress_window = Toplevel(self.parent.window)
		
		#Try it
		self.work_progress_window.resizable(height=False, width=False)
		#Try it end
		
#		self.work_progress_window.geometry('250x90')
		self.work_progress_window.title(f'Working...')
		self.work_progress_window.transient(self.parent.window)
		self.work_progress_window.wait_visibility()
		self.work_progress_window.grab_set()
		
		work_progress_frame = ttk.Frame(self.work_progress_window, padding='5 5', width=250)
		work_progress_frame.columnconfigure(0, weight=1, minsize=250)
		work_progress_frame.rowconfigure(0, weight=1)
		work_progress_frame.grid(column=0, row=0, sticky='swen')
		
		self.work_progress_label = ttk.Label(work_progress_frame, text='Working...')
		self.work_progress_label.grid(column=0, row=0, sticky='w', pady=5)
		
		#TODO : improve progress bar
		self.work_progress_progressbar = ttk.Progressbar(work_progress_frame, orient='horizontal', mode='indeterminate', length=250)
		###
		self.work_progress_progressbar.grid(column=0, row=1, pady=5)
		
		self.work_progress_button = ttk.Button(work_progress_frame, text='Interrupt!', padding='5 5', command=lambda: WorkProgressCallbacks.interrupt(self))
		self.work_progress_button.grid(column=0, row=2, sticky='sn')
		###END
		
		#SCHEDULE first update
		self.work_progress_window.after(100, lambda: WorkProgressCallbacks.update(self))
		###
		
		self.work_progress_progressbar.start(5)
		
		return self.work_progress_window
		
		
	def _destroy_window_sequence(self):
		WorkProgressCallbacks.done(self)
		self.work_progress_window.destroy()
		#TODO : SOMETHING WRONG HAPPENS ???? or not......
#		self.parent_x_button_deactivate()
