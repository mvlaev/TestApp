from tkinter import *
from tkinter import ttk

from GUI.program_windows.program_window import ProgramWindow
from GUI.callbacks.show_log_callbacks   import ShowLogCallbacks

class ShowLogWindow(ProgramWindow):
	def __init__(self, parent, controller, log_type):
		self.log_type = log_type
		super().__init__(parent, controller)
		
	
	def x_button_deactivate(self):
		self.show_log_window.protocol('WM_DELETE_WINDOW', lambda: None)
		
	
	def x_button_activate(self):
		self.show_log_window.protocol("WM_DELETE_WINDOW", self.close_window)
		
	
	def _create_window(self):
		'''Creates the program window.
		
		Must create one and only one window.
		Must return the tkinter.Tk() instance of that window
		'''
		
		
		self.show_log_window = Toplevel(self.parent.window)
		#show_log_window.transient(self.work_well_done_window)
		self.show_log_window.wait_visibility()
		self.show_log_window.grab_set()
		#self.show_log_window.resizable(height=False, width=False)
		self.show_log_window.title('Log File')
		self.show_log_window.columnconfigure(0, weight=1)
		self.show_log_window.rowconfigure(0, weight=1)
		
		
		show_log_frame = ttk.Frame(self.show_log_window, width=250, height=100, padding='5 5')
		show_log_frame.grid(column=0, row=0, sticky='swen')
		show_log_frame.columnconfigure(0, weight=1)
		show_log_frame.rowconfigure(0, weight=1)
		
		#Adding text widget
		show_log_text = Text(show_log_frame, wrap='word')
		show_log_text.grid(sticky='swen')
		
		#Importing the log content
		log = ShowLogCallbacks.get_log(self, self.log_type)
		show_log_text.insert('end', log)
		show_log_text['state'] = 'disabled'
		###
		
		if self.log_type == 'ControlLog': show_log_text.see('end')
		
		###Add scrollbars
		y_scroll = ttk.Scrollbar(show_log_frame, orient='vertical', command=show_log_text.yview)
		y_scroll.grid(column=1, row=0, sticky='ns')
		
		show_log_text['yscrollcommand'] = y_scroll.set
		###
		
		
		self.save_as_button = ttk.Button(show_log_frame, text='Save As...', command=lambda: ShowLogCallbacks.save_log_as(self, self.log_type))
		self.save_as_button.grid(column=0, row=1, pady=5)
		
		self.master_withdraw()
		self.show_log_window.focus_set()
		return self.show_log_window
	
	def _destroy_window_sequence(self):
		self.master_show()
		self.parent.window.grab_set()
		self.parent.window.lift()
		self.show_log_window.destroy()
