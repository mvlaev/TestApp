from tkinter import filedialog
from GUI.program_windows.show_log_window import ShowLogWindow

class SummaryCallbacks:
	def show_control_log(self):
		ShowLogWindow(self, self.controller, 'ControlLog')
	
	def show_results_log(self):
		###TODO: CALL SHOW LOG WINDOW
		ShowLogWindow(self, self.controller, 'ResultsLog')
		print('Showing log file')
		
	def set_values(self):
		###TODO: CALL CONTROLLER FOR A RUNTIME STATUS
		if self.controller.success:
			text='''\
Procedure finished.
'''
		else:
			self.summary_window.title('Summary: ERROR!')
			self.summary_results_button.state(['disabled'])
#			self.summary_save_as_button.state(['disabled'])
#			self.summary_close_button.configure(text='Close')
			text = '''\
Something wrong happened.
Procedure did not finish successfully.

Please contact your support team
with the ControlLog file for help.
'''
		self.summary_label.configure(text=text)
		
	###CALL THE CONTROLLER FOR A SUMMARY TO DISPLAY
		self.summary_info_label_frame_text.insert('1.0', self.controller.get_summary())
		self.summary_info_label_frame_text['state'] = 'disabled'
		
