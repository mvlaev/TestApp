# _Welcome to TestApp !_

## Introduction

TestApp is a specialized software application developed as a complement to an existing system for preparing data for accounting. The purpose of TestApp is to check if the data in the .xlsx files of the system is valid by running various types of tests. TestApp displays the errors in an easy to track and fix manner and provides various log files.

## Instructions

TestApp works in 3 steps:
* Step 1:
    - Choose a file containing the data.
    - Choose a month.
    - Choose tests (at least one of each column)
* Step 2:
    - Run the tests on the data provided.
    - After completing the tests TestApp informs you about the errors.
* Step 3:
    - Check the logs.
    - If you want you could save any of the log files with detailed information about the errors.
