from logging import getLogger
from tkinter import filedialog

from GUI.program_windows.work_progress_window import WorkProgressWindow
from GUI.program_windows.help_window          import HelpWindow

class RootCallbacks:
	def check_sufficient_options(self):
		if all([self.input_filename_var.get(), self.select_month_combobox_var.get()]) \
		   and\
		   all([any([self.database_select_var.get(), self.data_select_var.get()]),
			    any([self.electro_select_var.get(), self.water_select_var.get()])]
			):
			self.action_button.state(['!disabled'])
		else:
			self.action_button.state(['disabled'])
	
	def load_file(self):
		self.x_button_deactivate()
		###DONE!: ARRANGE CORRECT FILETYPES!
		input_filename = filedialog.askopenfilename(filetypes={'{File Extension} {.xlsx}'})
		
		
		if input_filename: 
		###TODO : ENGAGE controller and do tests before returning
			self.input_filename_var.set(input_filename)
		
		self.x_button_activate()
	
	def start_tests(self):
		logger = getLogger('GUI.root.call')
		logger.info('Preparing kwargs for procedure...')
		
		prev_month = self.select_month_combobox_dict[self.select_month_combobox_var.get()]
		
		kwargs = dict(do_data_tests     = self.data_select_var.get(),
					  do_database_tests = self.database_select_var.get(),
					  do_water_tests    = self.water_select_var.get(),
					  do_electro_tests  = self.electro_select_var.get(),
					  input_filename    = self.input_filename_var.get(),
					  prev_month        = prev_month)
		
		
		###Passing the data to controller and starting procedure
		logger.info('Starting procedure...')
		self.controller.start_procedure(**kwargs)
		
		###TODO : setup the work in progress window
		WorkProgressWindow(self, self.controller)
		
		
		
	def help(self):
		HelpWindow(self, self.controller)
	
