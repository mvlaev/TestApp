from GUI.program_windows.summary_window import SummaryWindow

class WorkProgressCallbacks:
	def update(self):
		if not self.controller.done:
			self.work_progress_window.after(100, lambda: WorkProgressCallbacks.update(self))
			return
		
#		self.work_progress_progressbar.stop()
#		self.work_progress_progressbar.grid_remove()
#		self.work_progress_label.configure(text='Work done.\nPress OK to continue.')
#		self.work_progress_window.title('Done')
#		self.work_progress_button.configure(text='Ok', command=lambda: WorkProgressCallbacks.done(self))
		
		#Close the work_progress_window
		try:
			self.close_window()
		except Exception as err:
			print(err)
		else:
			print('Closing work_progress_window.')
		
	
	def interrupt(self):
	#TODO : IMPLEMENT THE INTERRUPT
		self.work_progress_button.state(['disabled'])
		self.controller.stop_procedure()
		
		while not self.controller.done:
			sleep(0.1)
		
		if not self.controller.success:
			self.work_progress_label.configure(text='Errors ocurred!')
		else:
			self.work_progress_label.configure(text='Procedure stopped successfully.')
		
		self.work_progress_window.title('Done')
		self.work_progress_button.configure(text='Ok', command=lambda: WorkProgressCallbacks.done(self))
		self.work_progress_button.state(['!disabled'])
#		self.close_window()
	
	def done(self):
		
		###TODO : SOMETHING WRONG HAPPENS?????????
#		print(f'Calling the save as window with pars: {type(self.parent).__name__} {self.controller}')
		try:
			SummaryWindow(self.parent, self.controller)
		except Exception as err:
			print(err)
		else:
			print('Drawing SummaryWindow')

