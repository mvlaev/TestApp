from abc import ABC, abstractmethod
from Logic.containers.test_result import TestResult

class Test(ABC):
###TODO : WRITE A DOCSTRING
	'''Write a nice docstring here!
	
	Come on!
	'''
	_DEFAULT_OK_MESSAGE = 'OK'
	
	def __init__(self, data): #data should be class Data instance
		self.data = data
		self.result = TestResult(self.test_name, self.test_type, data)
	
	def start_test(self):
		
		###This should be caught by zero_tests!!!!!!!!!!!
		if self.data.data_id == None:
			self.result.test_status = 'FAIL'
			self.result.custom_message = f'FAIL! Test "{self.name}" got a faulty data_id!'
			return self.result
		###########
		
		self.result.test_status = self.calculate()
		if not type(self.result.test_status).__name__ == 'str':
			self.result.test_status = 'FAIL'
			self.result.custom_message = f'FAIL! Test "{self.name}" could not return a valid status.'
			return self.result
		
		self.result.custom_message = self.create_custom_message()
		if not type(self.result.custom_message).__name__ == 'str':
			self.result.custom_message = f'WARNING! Test "{self.name}" could not return a valid message. Original status was: {self.result.test_status}'
			self.result.test_status = 'FAIL'
			return self.result
		
		return self.result
	
	@property
	@abstractmethod
	def test_name(self):	pass
	
	@property
	@abstractmethod
	def test_type(self):	pass
	
	###Abstract methods
	@abstractmethod
	def calculate(self):
		'''Wraps all the calculations.
		
		MUST RETURN A TEST_STATUS_VALUE STRING.
		'''
		pass
	@abstractmethod
	def create_custom_message(self):
		'''Creates a custom status message for all possible statuses of the test.
		
		MUST RETURN A STRING.
		'''
		pass
