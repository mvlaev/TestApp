from logging         import getLogger
from multiprocessing import Process, Queue, set_start_method
from time            import asctime

from GUI.program_windows.root_window   import RootWindow

from Logic.controllers.control_logger  import master_logger, client_configurer
from Logic.controllers.main_controller import MainController

###Client Logger test code:
def client(q):
	client_configurer(q)
	return getLogger(__name__)
###########################################

def start_logger(q):
	logger = Process(target=master_logger, args=(q,))
	logger.start()
	
	return logger

def stop_logger(q, logger):
	q.put('STOP')
	logger.join()
	logger.close()

def main():
	q = Queue()
	logger_proc = start_logger(q)
	client_configurer(q)
	logger = getLogger(__name__)
	
	logger.info(f'Logger process started at {asctime()}.')
	logger.info('Starting the MainController...')
	mc = MainController(q)
	
	logger.info('Starting the GUI...')
	RootWindow(controller = mc)
	
	#End of main
	logger.info(f'Stopping the logger at {asctime()}, bye!')
	stop_logger(q, logger_proc)
	##############
	

if __name__ == '__main__':
	set_start_method('spawn')
	main()
