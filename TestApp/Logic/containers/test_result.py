class TestResult:
	def __init__(self, test_name, test_type, data):
		self.test_name = test_name
		self.test_type = test_type
		self.data_id = data.data_id
		self.test_status = None
		self.custom_message = 'No custom message.'
		
	def __str__(self):
		if not self.test_status:
			return f'Test "{self.test_name}" says: Ready to go.'
		return f'|Line: {self.data_id:<4d}|{f"Name: {self.test_name}":31s}|Type: {self.test_type:20s}|Status: {self.test_status:5s}|Test "{self.test_name}" says: {self.custom_message}'
	

