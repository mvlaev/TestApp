from tkinter import *
from tkinter import ttk
from datetime import date

from GUI.program_windows.program_window import ProgramWindow
from GUI.callbacks.root_callbacks import RootCallbacks


class RootWindow(ProgramWindow):
	#RootWindow initial text values:
	root_title = 'Test App'
	label_txt = '''Welcome!
		
This program will run tests on your data.
For help press Help.'''
	
	file_label_frame_txt = 'Select File'
	file_label_frame_label_txt = 'File Selected:'
	file_label_frame_button_txt = 'Choose File...'

	month_label_frame_txt = 'Select Month'

	options_label_frame_txt = 'Select Options'
	options_label_frame_database_select_txt = 'DataBase Tests'
	options_label_frame_readings_select_txt = 'Readings Tests'
	options_label_frame_electro_select_txt = 'Electro Tests'
	options_label_frame_water_select_txt = 'Water Tests'
	
	start_button_txt = 'Start Tests'
	cancel_button_txt = 'Close'
	help_button_txt = 'Help'
	
	def __init__(self, parent = None, controller = None):
		super().__init__(parent, controller)
		self.root.mainloop()
	
	
	def x_button_deactivate(self):
		self.root.protocol('WM_DELETE_WINDOW', lambda: None)
		
	def x_button_activate(self):
		self.root.protocol('WM_DELETE_WINDOW', self.close_window)
		
	
	def _create_window(self):
########Creating the root parent window:
		self.root = Tk()
		self.root.minsize(450, 400)
		self.root.title(self.root_title)
		self.root.columnconfigure(0, weight=1, minsize=50)
		self.root.rowconfigure(0, weight=1, minsize=25)
		############
		
		
		#Creating the main root window frame:
		frame = ttk.Frame(self.root)
		frame['padding'] = (5, 5)
		#apply styles to frame.......?
		
		for c in range(9): frame.columnconfigure(c, weight=1)
#		frame.columnconfigure(1, weight=1)
#		frame.columnconfigure(2, weight=1)
#		frame.columnconfigure(3, weight=1)
#		frame.columnconfigure(4, weight=1)
		
		
		frame.rowconfigure(0, weight=1)
		frame.rowconfigure(1, weight=1)
		frame.rowconfigure(2, weight=1)
		frame.grid(column=0, row=0, sticky='wesn')
		###
		
		
		#Creating the root window label:
		label = ttk.Label(frame, text=self.label_txt, padding='5 5', justify='center')
		label.grid(column=0, row=0, columnspan=9, sticky='')
		###
		
		#Creating the inner frame, responsible for the labelframe sections of the main frame:
		inner_frame = ttk.Frame(frame, padding='5 5')
		inner_frame.columnconfigure(0, weight=1)
		
		rows = 3
		for r in range(rows):
			inner_frame.rowconfigure(r, weight=1)
			
		inner_frame.grid(column=0, row=1, columnspan=9, sticky='swen')
		###
		
		
#########Creating the labelframes for the inner frame:
		
		#File input frame
		file_label_frame = ttk.LabelFrame(inner_frame, text=self.file_label_frame_txt, padding='5 5 5 5')
		file_label_frame.columnconfigure(0, weight=1)
		file_label_frame.rowconfigure(1, weight=1)
		file_label_frame.grid(column=0, row=0, sticky='ew', padx=5, pady = 5)
		ttk.Label(file_label_frame, text=self.file_label_frame_label_txt).grid(column=0, row=0, sticky='wn', padx=5)
		###
		
		self.input_filename_var = StringVar(value='')
		self.input_filename_var.trace_add('write', lambda *args: RootCallbacks.check_sufficient_options(self))
		ttk.Label(file_label_frame, textvariable=self.input_filename_var, wraplength=320).grid(column=1, row=0)
		
		ttk.Button(file_label_frame, text=self.file_label_frame_button_txt, command=lambda *args: RootCallbacks.load_file(self)).grid(column=0, row=1, columnspan=2, pady=5)
		###
		
		#Month label frame
		month_label_frame = ttk.LabelFrame(inner_frame, text=self.month_label_frame_txt, padding='5 5 5 5')
		month_label_frame.columnconfigure(0, weight=1)
		month_label_frame.rowconfigure(0, weight=1)
		month_label_frame.grid(column=0, row=1, sticky='ew', padx=5, pady = 5)
		
		self.select_month_combobox_dict = dict(zip(('December',
													'January',
													'February',
													'March',
													'April',
													'May',
													'June',
													'July',
													'August',
													'September',
													'October',
													'November'),
													range(12)
												))
		
		self.select_month_combobox_var = StringVar()
		select_month_combobox = ttk.Combobox(month_label_frame, textvariable=self.select_month_combobox_var, state='readonly')
		select_month_combobox['values'] = tuple(self.select_month_combobox_dict.keys())
		select_month_combobox.current(date.today().month - 1)
		select_month_combobox.grid(column=0, row=0, pady=5)
		
		select_month_combobox.bind('<<ComboboxSelected>>', lambda *args: RootCallbacks.check_sufficient_options(self))
		###
		
		#Options label frame
		options_label_frame = ttk.LabelFrame(inner_frame, text=self.options_label_frame_txt, padding='5 5 5 5')
		options_label_frame.columnconfigure(0, weight=1)
		options_label_frame.columnconfigure(1, weight=1)
		options_label_frame.columnconfigure(2, weight=1)
		options_label_frame.rowconfigure(0, weight=1)
		options_label_frame.grid(column=0 , row=2, sticky='ew', padx=5, pady = 5)

		self.database_select_var = BooleanVar(value=False)
		self.data_select_var     = BooleanVar(value=False)
		self.electro_select_var  = BooleanVar(value=False)
		self.water_select_var    = BooleanVar(value=False)
		
		database_select = ttk.Checkbutton(options_label_frame,
											  variable=self.database_select_var,
											  text=self.options_label_frame_database_select_txt,
											  command=lambda: RootCallbacks.check_sufficient_options(self),
											  onvalue=True, offvalue=False)
		readings_select = ttk.Checkbutton(options_label_frame,
											  variable=self.data_select_var,
											  text=self.options_label_frame_readings_select_txt,
											  command=lambda: RootCallbacks.check_sufficient_options(self),
											  onvalue=True, offvalue=False)
		electro_select  = ttk.Checkbutton(options_label_frame,
											  variable=self.electro_select_var,
											  text=self.options_label_frame_electro_select_txt,
											  command=lambda: RootCallbacks.check_sufficient_options(self),
											  onvalue=True, offvalue=False)
		water_select    = ttk.Checkbutton(options_label_frame,
											  variable=self.water_select_var,
											  text=self.options_label_frame_water_select_txt,
											  command=lambda: RootCallbacks.check_sufficient_options(self),
											  onvalue=True, offvalue=False)
		
		
		database_select.grid(column=0, row=0, sticky='w')
		readings_select.grid(column=0, row=1, sticky='w')
		electro_select.grid(column=2, row=0, sticky='w')
		water_select.grid(column=2, row=1, sticky='w')
		
		ttk.Separator(options_label_frame, orient='vertical').grid(column=1, row=0, rowspan=2, padx=25, sticky='swen')
		
		ttk.Separator(frame, orient='horizontal').grid(column=0, row=2, sticky='swen', columnspan=9)
#####################
		
########Creating buttons for the main window frame
		self.action_button = ttk.Button(frame, text=self.start_button_txt, command=lambda: RootCallbacks.start_tests(self))
		self.action_button.state(['disabled'])
		self.action_button.grid(column=0, row=2, sticky='we', padx=9, pady=10)
		
		cancel_button = ttk.Button(frame, text=f'{self.cancel_button_txt}', command=self.close_window)
		cancel_button.grid(column=7, row=2, sticky='we', pady=10)
		
		help_button = ttk.Button(frame, text=f'{self.help_button_txt}', command=lambda: RootCallbacks.help(self))
		help_button.grid(column=8, row=2, sticky='we', padx=9, pady=10)
############
		
		return self.root
	
	def _destroy_window_sequence(self):
		self.root.quit()
	
