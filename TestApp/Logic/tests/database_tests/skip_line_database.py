from Logic.tests.test import Test

class SkipLineDatabase(Test):
	test_name = 'Skip Line Database Test'
	test_type = 'SkipLineDatabase'
	
	def calculate(self):
		if self.data.base.count(None) == len(self.data.base):
			return 'SKIP'
		
		return 'OK'
	
	def create_custom_message(self):
		if self.result.test_status == 'SKIP':
			return 'Skipping line.'
		return self._DEFAULT_OK_MESSAGE
