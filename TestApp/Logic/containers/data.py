from abc import ABC, abstractmethod

class Data(ABC):
	###Abstracts
	@abstractmethod
	def __init__(self): pass
	@abstractmethod
	def _feed(self):
		'''Constructs the __return_dict.
		
		__return_dict must be accessible from the properties of the Data class.
		__return_dict must have all the defined properties as keys
		'''
		pass
		
	@property
	@abstractmethod
	def BASE_DATA_LABELS(self): pass
	###
	
	###Common properties
	@property
	def data_id(self):
		return self._return('data_id')	
	@property
	def unit(self):
		return self._return('unit')
	@property
	def source(self):
		return self._return('source')
	@property
	def level(self):
		return self._return('level')
	@property
	def charge(self):
		return self._return('charge')
	@property
	def contract(self):
		return self._return('contract')
	@property
	def min(self):
		return self._return('min')
	@property
	def max(self):
		return self._return('max')
	@property
	def base(self):
		return self._return('base')
	@property
	def months(self):
		return self._return('months')
	@property
	def comments(self):
		return self._return('comments')
	@property
	def last_month(self):
		return self._return('last_month')
	@property
	def previous_month(self):
		return self._return('previous_month')
	###
	
	###Common return function
	def _return(self, what):
		return self._return_dict[what] 
	###
	
###TODO: GET THE DATA CLASSES DONE!!!!!!!!!!!!!!!!!!!
class ElectroData(Data):
	__BASE_DATA_LABELS = ('unit', 'source', 'level', 'charge', 'transform', 'group', 'group_part', 'contract', 'min', 'max')
	
	def __init__(self, data, prev_month):
		self.__prev_month = prev_month
		self._feed(data)
	
	@property
	def BASE_DATA_LABELS(self):
		return self.__BASE_DATA_LABELS
	
	###Additional electro properties
	@property
	def transform(self):
		return self._return('transform')
	@property
	def group(self):
		return self._return('group')
	@property
	def group_part(self):
		return self._return('group_part')
	###
	
	def _feed(self, data):
		'''Recieves and stores the data.
		
		data should be of the form: data = [data_id, (raw_data[0], raw_data[1], .......)]
		'''
		
		data_id = data[0]
		
		raw_data = data[1]
		
		base_data_list = raw_data[:10]
		
		base_data_tuples = tuple(zip(self.BASE_DATA_LABELS, base_data_list))
		
		months = raw_data[10:23]
		
		comments = raw_data[23:25]
		
		self._return_dict = dict([*base_data_tuples],
									data_id = data_id,
									base = base_data_list,
									months = months,
									comments = comments,
									previous_month = months[self.__prev_month],
									last_month = months[self.__prev_month + 1]
									)
	
class WaterData(Data):
	__BASE_DATA_LABELS = ('unit', 'source', 'level', 'charge', 'contract', 'min', 'max')
	
	def __init__(self, data, prev_month):
		self.__prev_month = prev_month
		self._feed(data)
	
	@property
	def BASE_DATA_LABELS(self):
		return self.__BASE_DATA_LABELS
	
	def _feed(self, data):
		'''Recieves and stores the data.
		
		data should be of the form: data = [data_id, (raw_data[0], raw_data[1], .......)]
		'''
		
		data_id = data[0]
		
		raw_data = data[1]
		
		base_data_list = raw_data[:7]
		
		base_data_tuples = tuple(zip(self.BASE_DATA_LABELS, base_data_list))
		
		months = raw_data[7:20]
		
		comments = raw_data[20:22]
		
		self._return_dict = dict([*base_data_tuples],
									data_id = data_id,
									base = base_data_list,
									months = months,
									comments = comments,
									previous_month = months[self.__prev_month],
									last_month = months[self.__prev_month + 1]
									)

