from Logic.containers.results_container import ResultsContainer
from Logic.containers.tests_container import TestsContainer
###

def task(tests_container, line, __do_data_tests, __do_database_tests):
	def __bad_line(results):
		for r in results:
			if not r.test_status == 'OK':
				return True
		return False
			
	def __run_tests(line, test_classes):
		done = [test(line).start_test() for test in test_classes]
		return done
	
	
	local_results = []
	
	###ALWAYS Do critical_tests
	critical_results = __run_tests(line, tests_container.critical_test_classes)
	local_results += critical_results
	
	if __bad_line(critical_results):
		return local_results
		
	###If critical tests are OK:
	#Check if to do data tests
	if __do_data_tests:
		data_tests_results = __run_tests(line, tests_container.skip_line_data_classes)
		local_results += data_tests_results
		
		if not __bad_line(data_tests_results):
			###If not skipping
			#DO data tests
			local_results += __run_tests(line, tests_container.data_test_classes)
	
	#Check if to do database tests
	if __do_database_tests:
		database_tests_results = __run_tests(line, tests_container.skip_line_database_classes)
		local_results += database_tests_results
		
		if not __bad_line(database_tests_results):
			###If not skipping
			#Do database tests
			local_results += __run_tests(line, tests_container.database_test_classes)
	
	return local_results

class TestsController:
	def __init__(self, *, do_data_tests, do_database_tests, data_type):
		'''Recieves data_container, and control flags.
		
		data_type         - type: str, one of DATA_TYPES
		do_data_tests	  - type: bool
		do_database_tests - type: bool
		'''
		
		#Internal resources:
		self.__tests_container   = TestsContainer(data_type)
		self.__results_container = ResultsContainer(data_type)
		self.__do_data_tests	 = do_data_tests
		self.__do_database_tests = do_database_tests
	
	@property
	def results(self):
		return self.__results_container
	
	def step(self, line):
		r = task(self.__tests_container, line, self.__do_data_tests, self.__do_database_tests)
		for i in r: self.__results_container.add_result(i)
#		with Pool() as p:
#			tasks = [p.apply_async(task, (self.__tests_container, line, self.__do_data_tests, self.__do_database_tests)) for line in self.__data_container]
#			for no, t in enumerate(tasks):
#				for r in t.get():
#					self.__results_container.add_result(r)
#				
#				print(f'Task No.: {no} done! Added to ResultsContainer.')

