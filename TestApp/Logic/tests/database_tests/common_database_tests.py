from Logic.tests.test import Test

class ValidMinValue(Test):
	test_name = 'Database Min Test'
	test_type = 'CommonDatabaseTest'
	
	def calculate(self):
		try:
			float(self.data.min)
		except TypeError:
		###min is None:
			if self.data.unit and not self.data.level:
				return 'None'
			return 'OK'
		except ValueError:
		###min is not a valid number:
			if self.data.min == 'база':
				return 'OK'
			return 'NAN'
		else:
			if self.data.min < 0:
				return 'NEGATIVE'
			return 'OK'
	
	def create_custom_message(self):
		if self.result.test_status == 'None':
			self.result.test_status = 'ERROR'
			return 'Min value missing!'
		
		if self.result.test_status == 'NAN':
			self.result.test_status = 'ERROR'
			return 'Min value has bad characters!'
		
		if self.result.test_status == 'NEGATIVE':
			self.result.test_status = 'ERROR'
			return 'Min value is NEGATIVE!'
		
		if self.result.test_status == 'OK':
			return self._DEFAULT_OK_MESSAGE
		
		self.result.test_status = 'FAIL'
		return 'UNKNOWN ERROR HAPPENED!'

class ValidMaxValue(Test):
	test_name = 'Database Max Test'
	test_type = 'CommonDatabaseTest'
	
	def calculate(self):
		try:
			float(self.data.max)
		except TypeError:
		###max is None:
			if self.data.unit and not self.data.level:
				return 'None'
			return 'OK'
		except ValueError:
		###max is not a valid number:
			return 'NAN'
		else:
			if self.data.max < 0:
				return 'NEGATIVE'
			return 'OK'
	
	def create_custom_message(self):
		if self.result.test_status == 'None':
			self.result.test_status = 'ERROR'
			return 'Max value missing!'
		
		if self.result.test_status == 'NAN':
			self.result.test_status = 'ERROR'
			return 'Max value has bad characters!'
		
		if self.result.test_status == 'NEGATIVE':
			self.result.test_status = 'ERROR'
			return 'Max value is NEGATIVE!'
		
		if self.result.test_status == 'OK':
			return self._DEFAULT_OK_MESSAGE
		
		self.result.test_status = 'FAIL'
		return 'UNKNOWN ERROR HAPPENED!'

